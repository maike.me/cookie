package cookie

import (
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/maike.me/config"
)

var cookies = make(map[string]*sessions.CookieStore)

func GetSession(r *http.Request, name string) (*sessions.Session, error) {
	if cookies[name] == nil {
		switch config.Viper("config").GetString("environment") {
		case "development":
			cookies[name] = sessions.NewCookieStore(
				[]byte(config.Viper("config").GetString("session.development.authenticationKey")),
				[]byte(config.Viper("config").GetString("session.development.encryptionKey")))
			cookies[name].Options = &sessions.Options{
				Domain: config.Viper("config").GetString("session.development.domain"),
				Path:   "/",
				MaxAge: config.Viper("config").GetInt("session.development.maxAge"),
			}
		case "production":
			cookies[name] = sessions.NewCookieStore(
				[]byte(config.Viper("config").GetString("session.production.authenticationKey")),
				[]byte(config.Viper("config").GetString("session.production.encryptionKey")))
			cookies[name].Options = &sessions.Options{
				Domain: config.Viper("config").GetString("session.production.domain"),
				Path:   "/",
				MaxAge: config.Viper("config").GetInt("session.production.maxAge"),
			}
		default:
			panic("./config.json environment not set to development or production")
		}
	}
	return cookies[name].Get(r, name)
}

func ClearSession(sess *sessions.Session) {
	for key := range sess.Values {
		delete(sess.Values, key)
	}
}
